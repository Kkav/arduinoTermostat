#include "Wire.h"
#define DS3231_I2C_ADDRESS 0x68


// the regular Adafruit "TouchScreen.h" library only works on AVRs

// different mcufriend shields have Touchscreen on different pins
// and rotation.
// Run the UTouch_calibr_kbv sketch for calibration of your shield
//LCD Ayar
#include <Adafruit_GFX.h>    // Core graphics library
#include <Fonts/FreeMono12pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/Org_01.h>

//#include <Adafruit_TFTLCD.h> // Hardware-specific library
//Adafruit_TFTLCD tft(A3, A2, A1, A0, A4);
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;       // hard-wired for UNO shields anyway.
#include <TouchScreen.h>

#if defined(__SAM3X8E__)
#undef __FlashStringHelper::F(string_literal)
#define F(string_literal) string_literal
#endif
// most mcufriend shields use these pins and Portrait mode:
uint8_t YP = A1;  // must be an analog pin, use "An" notation!
uint8_t XM = A2;  // must be an analog pin, use "An" notation!
uint8_t YM = 7;   // can be a digital pin
uint8_t XP = 6;   // can be a digital pin
uint8_t SwapXY = 0;


uint16_t TS_LEFT = 920;
uint16_t TS_RT  = 150;
uint16_t TS_TOP = 940;
uint16_t TS_BOT = 120;

String Gunler[]={"x","PAZAR","PAZARTESI","SALI","CARSAMBA","PERSEMBE","CUMA","CUMARTESI"};
char *name = "Unknown controller";
extern uint8_t SmallFontFreeSevenSegNumFont[];
// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);
TSPoint tp;

#define MINPRESSURE 20
#define MAXPRESSURE 1000

#define SWAP(a, b) {uint16_t tmp = a; a = b; b = tmp;}

uint16_t identifier;
uint8_t Orientation = 0; 

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GRAY    0x8410
//LCD Ayar
//Menu Degisken
int Menugecis=0;//0 ana menu 1 ayarlar kısmı
int saathaf=1000;
int gunhaf=1000;
byte saniye=1000;
byte dakika=1000;
byte asaat=1000;
byte aygunu=1000;
byte ay=1000;
byte yil=1000;
int secTarih=0;
int secDerece=0;

//sicaklık
#include <OneWire.h>
// OneWire DS18S20, DS18B20, DS1822 Temperature Example
//
// http://www.pjrc.com/teensy/td_libs_OneWire.html
//
// The DallasTemperature library can do all this work for you!
// http://milesburton.com/Dallas_Temperature_Control_Library

OneWire  ds(22);  // on pin 10 (a 4.7K resistor is necessary)
float celsius, fahrenheit;
//sicaklık



// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}
// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}

///
int HaftaninGunu=-1;
int saat=-1;
float SicakliS=-1000;
float sicakdeger=-1000;
float Derece[3][8][24];


void LCDilkAyar()
{   uint16_t tmp;
    tft.reset();
    identifier = tft.readID();
    //    if (identifier == 0) identifier = 0x9341;
    if (0) {
    } else if (identifier == 0x0154) {
        name = "S6D0154";
        TS_LEFT = 914; TS_RT = 181; TS_TOP = 957; TS_BOT = 208;
    } else if (identifier == 0x5408) {  //thanks gazialankus
        name = "SPFD5408";
        TS_LEFT = 150; TS_RT = 960; TS_TOP = 155; TS_BOT = 925;
        SwapXY = 1;
    } else if (identifier == 0x7783) {
        name = "ST7781";
        TS_LEFT = 865; TS_RT = 155; TS_TOP = 942; TS_BOT = 153;
        SwapXY = 1;
    } else if (identifier == 0x7789) {
        name = "ST7789V";
        YP = A2; XM = A1; YM = 7; XP = 6;
        TS_LEFT = 906; TS_RT = 169; TS_TOP = 161; TS_BOT = 919;
    } else if (identifier == 0x9320) {
        name = "ILI9320";
        YP = A3; XM = A2; YM = 9; XP = 8;
        TS_LEFT = 902; TS_RT = 137; TS_TOP = 941; TS_BOT = 134;
    } else if (identifier == 0x9325) {
        name = "ILI9325";
        TS_LEFT = 900; TS_RT = 103; TS_TOP = 96; TS_BOT = 904;
    } else if (identifier == 0x9325) {
        name = "ILI9325 Green Dog";
        TS_LEFT = 900; TS_RT = 130; TS_TOP = 940; TS_BOT = 130;
    } else if (identifier == 0x9327) {
        name = "ILI9327";
        TS_LEFT = 899; TS_RT = 135; TS_TOP = 935; TS_BOT = 79;
        SwapXY = 1;
    } else if (identifier == 0x9329) {
        name = "ILI9329";
        TS_LEFT = 143; TS_RT = 885; TS_TOP = 941; TS_BOT = 131;
        SwapXY = 1;
    } else if (identifier == 0x9341) {
        name = "ILI9341 BLUE";
        TS_LEFT = 920; TS_RT = 139; TS_TOP = 944; TS_BOT = 150;
        SwapXY = 0;
    } else if (identifier == 0) {
        name = "ILI9341 DealExtreme";
        TS_LEFT = 893; TS_RT = 145; TS_TOP = 930; TS_BOT = 135;
        SwapXY = 1;
    } else if (identifier == 0 || identifier == 0x9341) {
        name = "ILI9341";
        TS_LEFT = 128; TS_RT = 911; TS_TOP = 105; TS_BOT = 908;
        SwapXY = 1;
    } else if (identifier == 0x9486) {
        name = "ILI9486";
        TS_LEFT = 904; TS_RT = 170; TS_TOP = 950; TS_BOT = 158;
    } else if (identifier == 0x9488) {
        name = "ILI9488";
        TS_LEFT = 904; TS_RT = 170; TS_TOP = 950; TS_BOT = 158;
    } else if (identifier == 0xB509) {
        name = "R61509V";
        TS_LEFT = 889; TS_RT = 149; TS_TOP = 106; TS_BOT = 975;
        SwapXY = 1;
    } else {
        name = "unknown";
    }
    switch (Orientation) {      // adjust for different aspects
        case 0:   break;        //no change,  calibrated for PORTRAIT
        case 1:   tmp = TS_LEFT, TS_LEFT = TS_BOT, TS_BOT = TS_RT, TS_RT = TS_TOP, TS_TOP = tmp;  break;
        case 2:   SWAP(TS_LEFT, TS_RT);  SWAP(TS_TOP, TS_BOT); break;
        case 3:   tmp = TS_LEFT, TS_LEFT = TS_TOP, TS_TOP = TS_RT, TS_RT = TS_BOT, TS_BOT = tmp;  break;
    }

    Serial.begin(9600);
    ts = TouchScreen(XP, YP, XM, YM, 300);     //call the constructor AGAIN with new values.
   
    tft.begin(identifier);
    tft.setRotation(3);
    tft.fillScreen(GRAY);
    tft.setFont(&Org_01);
    }
void Versiyon()
{
tft.fillRect(132, 10, 320, 40,GRAY);
  
tft.setFont(&FreeMonoBold9pt7b);
tft.setTextSize(1);
tft.setTextColor(YELLOW); 
tft.setCursor(165, 35); 
tft.print("KOMBI TERMOSTATI");

tft.setFont(&FreeMonoBold9pt7b);
tft.fillRect(365, 20, 85, 22,RED);
tft.setTextSize(1);
tft.setTextColor(YELLOW); 
tft.setCursor(370, 35); 
tft.print("AYARLAR");
}
void setup()
{
  Wire.begin();
  Serial.begin(9600);
  // set the initial time here:
  // DS3231 seconds, minutes, hours, day, date, month, year
 //setDS3231time(00,9,1,2,12,2,18);
//Geçici
for(int i=1;i<8;i++)
{for(int x=0;x<24;x++)
{
  Derece[0][i][x]=27;
}}
 
LCDilkAyar();
Versiyon();
tft.drawRect(123, 60, 330, 180,YELLOW);
tft.drawRect(125, 62, 326, 176,RED);

  Saat();
  Gun(); 
}
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
dayOfMonth, byte month, byte year)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
}
void readDS3231time(byte *second,
byte *minute,
byte *hour,
byte *dayOfWeek,
byte *dayOfMonth,
byte *month,
byte *year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}
void displayTime()
{
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  // retrieve data from DS3231
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  int HaftaninGunuS;
//Yazı Büyüklügü ve Yazı renkleri

 if(asaat !=hour|| dakika !=minute)//Gereksiz yenileme yapma
 {
 tft.setFont(&Org_01);
 tft.setTextSize(5);
 tft.setTextColor(YELLOW);
 tft.setCursor(10, 30);
 tft.fillRect(0, 5, 135, 35,GRAY);//Ekran temizle
  // send it to the serial monitor
  //Serial.print(hour, DEC);
 
  tft.print(hour,DEC); 
  // convert the byte variable to a decimal number when displayed
  //Serial.print(":");
   tft.print(":");
  if (minute<10)
  {
    //Serial.print("0");
    tft.print("0");
  }
  //Serial.print(minute, DEC);
  tft.print(minute,DEC); 
   asaat =hour;
   dakika=minute;
 }

if(aygunu !=dayOfMonth || ay !=month  || yil !=year )//Gereksiz yenileme yapma
 {
tft.fillRect(0, 40, 120, 22,GRAY);//Ekran temizle
 tft.setTextSize(1);
 tft.setTextColor(GREEN);
 tft.setFont(&FreeMonoBold9pt7b);
 tft.setCursor(10, 55);
  //Serial.print(dayOfMonth, DEC);
  tft.print(dayOfMonth, DEC);
  //Serial.print("/");
  tft.print("/");
 // Serial.print(month, DEC);
  tft.print(month, DEC);
  //Serial.print("/");
  tft.print("/");
 // Serial.print(year, DEC);
  tft.print(2000+year, DEC);
  //Serial.println(" Day of week: ");
  //tft.print("0");
   tft.println(" ");
   tft.print(" ");
   tft.setTextColor(BLUE);
   aygunu=dayOfMonth;
    ay =month;
    yil=year;
 }

   
  switch(dayOfWeek){
  case 1:
   // Serial.println("Pazar");
    //tft.print("Pazar");
    HaftaninGunuS=1;
    break;
  case 2:
    //Serial.println("Pazartesi");
    //tft.print("PAZARTESI");
    HaftaninGunuS=2;
    break;
  case 3:
    //Serial.println("Salı");
    //tft.print("SALI");
    HaftaninGunuS=3;
    break;
  case 4:
    //Serial.println("Çarşamba");
    //tft.print("CARSAMBA");
    HaftaninGunuS=4;
    break;
  case 5:
    //Serial.println("Perşembe");
    //tft.print("PERSEMBE");
    HaftaninGunuS=5;
    break;
  case 6:
    //Serial.println("Cuma");
    //tft.print("CUMA");
    HaftaninGunuS=6;
    break;
  case 7:
    //Serial.println("Cumartesi");
    //tft.print("CUMARTESI");
    HaftaninGunuS=7;
    break;
  }

if (HaftaninGunu!=HaftaninGunuS) {GunSec(HaftaninGunuS,HaftaninGunu);HaftaninGunu=HaftaninGunuS;}
if (saat!=hour){SaatSecim(hour,saat);saat=hour;}

}

void BilgiYaz()
{
  // tft.setFont(&FreeMonoOblique24pt7b);
    tft.setTextSize(20);
   tft.setTextColor(YELLOW, RED);
    tft.setCursor(50, 50);
    tft.print("0123456789"); 
    // tft.setFont(&FreeMonoOblique24pt7b);
     tft.setTextSize(1);
   tft.print("0123456789"); 
  
  }



void Sicaklik()
{ byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];

  
  if ( !ds.search(addr)) {
   // Serial.println("No more addresses.");
    //Serial.println();
    ds.reset_search();
    delay(250);
    return;
  }
  
 /* Serial.print("ROM =");
  for( i = 0; i < 8; i++) {
    Serial.write(' ');
   Serial.print(addr[i], HEX);
  }*/

  if (OneWire::crc8(addr, 7) != addr[7]) {
      //Serial.println("CRC is not valid!");
      return;
  }
  //Serial.println();
 
  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      //Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
      //Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      //Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      //Serial.println("Device is not a DS18x20 family device.");
      return;
  } 

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

 /* Serial.print("  Data = ");
  Serial.print(present, HEX);
  Serial.print(" ");*/
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
   /* Serial.print(data[i], HEX);
    Serial.print(" ");*/
  }
 /* Serial.print(" CRC=");
  Serial.print(OneWire::crc8(data, 8), HEX);
  Serial.println();*/

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;


if(SicakliS!=celsius)
{  
SicakliS=celsius; 
tft.setFont(&FreeMonoBold12pt7b);
//tft.fillRect(126, 63, 325, 175,GRAY);//Ekran temizle
tft.fillRect(160, 120, 245,60,GRAY);//Ekran temizle
//tft.drawRect(160, 120, 245,60,YELLOW);//Ekran temizle
tft.setCursor(160, 160);
tft.setTextSize(2);
if(Derece[0][HaftaninGunu][saat]>SicakliS) tft.setTextColor(RED); else tft.setTextColor(GREEN);

tft.print(SicakliS),tft.print(" *C");
}


SicaklikO();
  }

  
void Gun()
{
 
tft.setFont(&FreeMonoBold9pt7b);
tft.setTextSize(1);
tft.setTextColor(BLUE); 
for(int i=1;i<8;i++)
{
tft.fillRect(8, 40+i*25, 110, 20,YELLOW);  
tft.setCursor(10, 55+i*25); 
tft.print(Gunler[i]);
 
}
}  
  void GunSec(int deger,int eskideger)
{

tft.setFont(&FreeMonoBold9pt7b);
tft.setTextSize(1);
tft.setTextColor(YELLOW); 
tft.fillRect(8, 40+deger*25, 110, 20,BLUE);  
tft.setCursor(10, 55+deger*25); 
tft.print(Gunler[deger]);

if(eskideger==-1) return;
tft.setTextSize(1);
tft.setTextColor(BLUE); 
tft.fillRect(8, 40+eskideger*25, 110, 20,YELLOW);  
tft.setCursor(10, 55+eskideger*25); 
tft.print(Gunler[eskideger]);
 
  }
void Saat()
{
   tft.setFont(&FreeMonoBold12pt7b);

tft.setTextSize(1);
tft.setTextColor(RED); 
for(int i=0;i<12;i++)
{

tft.fillRect(8+i*38, 245, 35, 25,WHITE);  
tft.setCursor(10+i*38, 265); 
tft.print(i);
tft.fillRect(8+i*38, 280, 35, 25,WHITE);  
tft.setCursor(10+i*38, 300); 
tft.print(i+12);
}  
  }

void SaatSecim(int deger,int eskideger)
{

   tft.setFont(&FreeMonoBold12pt7b);

tft.setTextSize(1);
tft.setTextColor(WHITE); 
 
if(deger<12)
{
tft.fillRect(8+deger*38, 245, 35, 25,RED);  
tft.setCursor(10+deger*38, 265); 
tft.print(deger);
}
else
{
tft.fillRect(8+(deger-12)*38, 280, 35, 25,RED);  
tft.setCursor(10+(deger-12)*38, 300); 
tft.print(deger);
}


//Silme
tft.setTextSize(1);
tft.setTextColor(RED); 
 if (eskideger==-1) return;
if(eskideger<12)
{
tft.fillRect(8+eskideger*38, 245, 35, 25,WHITE);  
tft.setCursor(10+eskideger*38, 265); 
tft.print(eskideger);
}
else
{
tft.fillRect(8+(eskideger-12)*38, 280, 35, 25,WHITE);  
tft.setCursor(10+(eskideger-12)*38, 300); 
tft.print(eskideger);
}
  }
  
void SicaklikO()
{
 if( sicakdeger != Derece[0][HaftaninGunu][saat])
{  
tft.setFont(&FreeMonoBold12pt7b);

tft.setCursor(320, 230);
tft.fillRect(320, 210, 120,25,RED);
tft.setTextSize(1);
tft.setTextColor(WHITE);
tft.print(Derece[0][HaftaninGunu][saat]),tft.print(" *C");
Serial.println(HaftaninGunu);
Serial.println(saat);
sicakdeger=Derece[0][HaftaninGunu][saat];
}
}

void loop()
{
  if(Menugecis==0)
  {
Sicaklik();
displayTime(); // display the real-time clock data on the Serial Monitor,
}
else  if(Menugecis==1)
{
Dokunmatik();
}
AyarSecim();//Ana menuyu seç
}
void AyarSecim()
{
  uint16_t xpos, ypos;  //screen coordinates
    tp = ts.getPoint();   //tp.x, tp.y are ADC values

    // if sharing pins, you'll need to fix the directions of the touchscreen pins
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
    pinMode(XP, OUTPUT);
    pinMode(YM, OUTPUT);
    //    digitalWrite(XM, HIGH);
    //    digitalWrite(YP, HIGH);
    // we have some minimum pressure we consider 'valid'
    // pressure of 0 means no pressing!

 
    if (tp.z > MINPRESSURE && tp.z < MAXPRESSURE) {
        // is controller wired for Landscape ? or are we oriented in Landscape?
        if (SwapXY != (Orientation & 1)) SWAP(tp.x, tp.y);
        // scale from 0->1023 to tft.width  i.e. left = 0, rt = width
        // most mcufriend have touch (with icons) that extends below the TFT
        // screens without icons need to reserve a space for "erase"
        // scale the ADC values from ts.getPoint() to screen values e.g. 0-239
        xpos = map(tp.x, TS_LEFT, TS_RT, 0,tft.height() );
        ypos = map(tp.y, TS_BOT-20 , TS_TOP, 0,tft.width() );
 if(xpos>21 && ypos>370&& xpos<44 && ypos<452)
 {
      if( Menugecis==1) 
     {
Menugecis=0;
Versiyon();
if(saathaf!=-1 && saat != saathaf) SaatSecim(saat,saathaf);//Eskiye dönüyoruz
if(gunhaf!=-1 && HaftaninGunu !=gunhaf)  GunSec(HaftaninGunu,gunhaf);//Eskiye dönüyoruz
      } 
      else 
     { 
Menugecis=1;
tft.fillRect(132, 10, 320, 40,GRAY);
tft.setFont(&FreeMonoBold9pt7b);
tft.fillRect(365, 20, 85, 22,RED);
tft.setTextSize(1);
tft.setTextColor(YELLOW); 
tft.setCursor(370, 35); 
tft.print(" MENU");


tft.fillRect(140, 20, 120, 22,RED);
tft.setTextSize(1);
tft.setTextColor(YELLOW); 
tft.setCursor(145, 35); 
tft.print("Saat/Tarih");

tft.fillRect(270, 20, 85, 22,RED);
tft.setTextSize(1);
tft.setTextColor(YELLOW); 
tft.setCursor(275, 35); 
tft.print("Derece");

saathaf=saat;
gunhaf=HaftaninGunu;
     }
      delay(500);//Tekrarlı seçimi iptal ediyoruz.  
    } 
    }
 
}



void Dokunmatik()
{
 uint16_t xpos, ypos;  //screen coordinates
    tp = ts.getPoint();   //tp.x, tp.y are ADC values

    // if sharing pins, you'll need to fix the directions of the touchscreen pins
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
    pinMode(XP, OUTPUT);
    pinMode(YM, OUTPUT);
    //    digitalWrite(XM, HIGH);
    //    digitalWrite(YP, HIGH);
    // we have some minimum pressure we consider 'valid'
    // pressure of 0 means no pressing!

    if (tp.z > MINPRESSURE && tp.z < MAXPRESSURE) {
        // is controller wired for Landscape ? or are we oriented in Landscape?
        if (SwapXY != (Orientation & 1)) SWAP(tp.x, tp.y);
        // scale from 0->1023 to tft.width  i.e. left = 0, rt = width
        // most mcufriend have touch (with icons) that extends below the TFT
        // screens without icons need to reserve a space for "erase"
        // scale the ADC values from ts.getPoint() to screen values e.g. 0-239
        xpos = map(tp.x, TS_LEFT, TS_RT, 0,tft.height() );
        ypos = map(tp.y, TS_BOT-20 , TS_TOP, 0,tft.width() );
        tft.setFont(NULL);
        tft.setTextColor(WHITE,GRAY);
        tft.setCursor(160, 190);
        tft.setTextSize(2);
        tft.print("tp.x=" + String(xpos) + " tp.y=" + String(ypos) + "   ");  
//Secim İşlemleri
//saathaf=saat;
//gunhaf=HaftaninGunu;
if(xpos>60 && ypos>10 && xpos<85 && ypos<116)   {GunSec(1,gunhaf);gunhaf=1;}
if(xpos>85 && ypos>10 && xpos<100 && ypos<116)  {GunSec(2,gunhaf);gunhaf=2;}
if(xpos>110 && ypos>10 && xpos<125 && ypos<116) {GunSec(3,gunhaf);gunhaf=3;}
if(xpos>135 && ypos>10 && xpos<155 && ypos<116) {GunSec(4,gunhaf);gunhaf=4;}
if(xpos>160 && ypos>10 && xpos<175 && ypos<116) {GunSec(5,gunhaf);gunhaf=5;};
if(xpos>185 && ypos>10 && xpos<200 && ypos<116) {GunSec(6,gunhaf);gunhaf=6;}
if(xpos>210 && ypos>10 && xpos<225 && ypos<116) {GunSec(7,gunhaf);gunhaf=7;}
int a=38;
if(xpos>234 && ypos>8+0*a && xpos<261 && ypos<8+0*a+35) {SaatSecim(0,saathaf);saathaf=0;}
if(xpos>234 && ypos>8+1*a && xpos<261 && ypos<8+1*a+35) {SaatSecim(1,saathaf);saathaf=1;}
if(xpos>234 && ypos>8+2*a && xpos<261 && ypos<8+2*a+35) {SaatSecim(2,saathaf);saathaf=2;}
if(xpos>234 && ypos>8+3*a && xpos<261 && ypos<8+3*a+35) {SaatSecim(3,saathaf);saathaf=3;}
if(xpos>234 && ypos>8+4*a && xpos<261 && ypos<8+4*a+35) {SaatSecim(4,saathaf);saathaf=4;}
if(xpos>234 && ypos>8+5*a && xpos<261 && ypos<8+5*a+35) {SaatSecim(5,saathaf);saathaf=5;}
if(xpos>234 && ypos>8+6*a && xpos<261 && ypos<8+6*a+35) {SaatSecim(6,saathaf);saathaf=6;}
if(xpos>234 && ypos>8+7*a && xpos<261 && ypos<8+7*a+35) {SaatSecim(7,saathaf);saathaf=7;}
if(xpos>234 && ypos>8+8*a && xpos<261 && ypos<8+8*a+35) {SaatSecim(8,saathaf);saathaf=8;}
if(xpos>234 && ypos>8+9*a && xpos<261 && ypos<8+9*a+35) {SaatSecim(9,saathaf);saathaf=9;}
if(xpos>234 && ypos>8+10*a && xpos<261 && ypos<8+10*a+35) {SaatSecim(10,saathaf);saathaf=10;}
if(xpos>234 && ypos>8+11*a && xpos<261 && ypos<8+11*a+35) {SaatSecim(11,saathaf);saathaf=11;}

if(xpos>270 && ypos>8+0*a && xpos<290 && ypos<8+0*a+35) {SaatSecim(12,saathaf);saathaf=12;}
if(xpos>270 && ypos>8+1*a && xpos<290 && ypos<8+1*a+35) {SaatSecim(13,saathaf);saathaf=13;}
if(xpos>270 && ypos>8+2*a && xpos<290 && ypos<8+2*a+35) {SaatSecim(14,saathaf);saathaf=14;}
if(xpos>270 && ypos>8+3*a && xpos<290 && ypos<8+3*a+35) {SaatSecim(15,saathaf);saathaf=15;}
if(xpos>270 && ypos>8+4*a && xpos<290 && ypos<8+4*a+35) {SaatSecim(16,saathaf);saathaf=16;}
if(xpos>270 && ypos>8+5*a && xpos<290 && ypos<8+5*a+35) {SaatSecim(17,saathaf);saathaf=17;}
if(xpos>270 && ypos>8+6*a && xpos<290 && ypos<8+6*a+35) {SaatSecim(18,saathaf);saathaf=18;}
if(xpos>270 && ypos>8+7*a && xpos<290 && ypos<8+7*a+35) {SaatSecim(19,saathaf);saathaf=19;}
if(xpos>270 && ypos>8+8*a && xpos<290 && ypos<8+8*a+35) {SaatSecim(20,saathaf);saathaf=20;}
if(xpos>270 && ypos>8+9*a && xpos<290 && ypos<8+9*a+35) {SaatSecim(21,saathaf);saathaf=21;}
if(xpos>270 && ypos>8+10*a && xpos<290 && ypos<8+10*a+35) {SaatSecim(22,saathaf);saathaf=22;}
if(xpos>270 && ypos>8+11*a && xpos<290 && ypos<8+11*a+35) {SaatSecim(23,saathaf);saathaf=23;}
// Tarih Secim
if(xpos>20 && ypos>150 && xpos<40 && ypos<265) {secTarih=1;
tft.setCursor(165, 100); 
tft.print("TARIH SECIM");}//Tarih secildi
if(xpos>20 && ypos>275 && xpos<40 && ypos<360) 
{secDerece=1;//Tarih secildi
tft.setCursor(165, 100); 
tft.print("DERECE SECIM");}

delay(500);//Tekrarlı seçimi iptal ediyoruz.        
    }

} 
  


